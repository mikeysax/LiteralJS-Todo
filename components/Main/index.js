/** @jsx h */
import { component, h } from 'literaljs';
import './index.scss';

// Components
import Logo from '../Logo';
import TodoCard from '../TodoCard';
import Footer from '../Footer';

export default component({
	name: 'Main',
	state: {
		todos: []
	},
	mounted() {
		this.loadTodos();
	},
	methods() {
		return {
			submit(e) {
				e.preventDefault();
				const newTodo = document.getElementById('text');
				this.setState(state => ({
					...state,
					todos: [
						...state.todos,
						{ id: new Date().valueOf(), text: newTodo.value }
					]
				}));
				newTodo.value = '';
				this.saveTodos();
			},
			loadTodos() {
				const savedTodos = localStorage.getItem('todos');
				if (savedTodos) {
					this.setState(state => ({
						...state,
						todos: [...JSON.parse(savedTodos)]
					}));
				}
			},
			updateTodos(index) {
				let { todos } = this.getState();
				todos[index]['text'] = document.getElementById('edit').value;
				this.setState(state => ({
					...state,
					todos: [...todos]
				}));
				this.saveTodos();
			},
			deleteTodo(index) {
				let { todos } = this.getState();
				todos.splice(index, 1);
				this.setStore(state => ({
					...state,
					todos: [...todos]
				}));
				this.saveTodos();
			},
			saveTodos() {
				const { todos } = this.getState();
				localStorage.setItem('todos', JSON.stringify(todos));
			}
		};
	},
	render() {
		const { todos } = this.getState();
		return (
			<div class="Main">
				<Logo />
				<form class="todo-form" events={{ submit: this.submit }}>
					<input
						id="text"
						type="text"
						placeholder="Enter A New Todo..."
					/>
				</form>
				{todos.map((todo, index) => (
					<TodoCard
						props={{
							key: todo.id,
							index,
							text: todo.text,
							deleteTodo: this.deleteTodo,
							updateTodos: this.updateTodos
						}}
					/>
				))}
				<Footer />
			</div>
		);
	}
});
