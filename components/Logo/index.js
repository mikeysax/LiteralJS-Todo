/** @jsx h */

import { component, h } from 'literaljs';
import './index.scss';

export default component({
	render() {
		return (
			<div class="Logo">
				<div class="letters">
					<div class="L">L</div>
					<div class="J">J</div>
					<div class="S">S</div>
				</div>
				<h1>
					<span class="Literal">Literal</span>
					<span class="JS">JS</span>
					<div class="todo">Todo App</div>
				</h1>
			</div>
		);
	}
});
