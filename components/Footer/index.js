/** @jsx h */

import { component, h } from 'literaljs';
import './index.scss';

export default component({
	render() {
		return (
			<div class="Footer">
				<div>
					<a href="https://gitlab.com/Mikeysax/LiteralJS-Todo">
						View Code
					</a>
				</div>
				<div>
					Built with <a href="https://literaljs.com/">LiteralJS</a>
				</div>
			</div>
		);
	}
});
