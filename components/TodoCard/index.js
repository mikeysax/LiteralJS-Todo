/** @jsx h */
import { component, h } from 'literaljs';
import './index.scss';

export default component({
	name: 'TodoCard',
	state: {
		edit: false
	},
	methods() {
		const { deleteTodo, updateTodos } = this.props;
		return {
			delete(index) {
				deleteTodo(index);
			},
			edit() {
				this.setState(state => ({
					edit: true
				}));
			},
			update(e, index) {
				e.preventDefault();
				updateTodos(index);
				this.setState({ edit: false });
			}
		};
	},
	render() {
		const { index, text } = this.props;
		const { edit } = this.getState();
		return !edit ? (
			<div class="Todo-Card">
				<p class="text">{text}</p>
				<button class="edit" events={{ click: () => this.edit() }}>
					Edit
				</button>
				<button
					class="delete"
					events={{ click: () => this.delete(index) }}
				>
					Delete
				</button>
			</div>
		) : (
			<div class="Todo-Card -edit">
				<form events={{ submit: e => this.update(e, index) }}>
					<input id="edit" type="text" value={text} />
					<button type="submit">Update</button>
				</form>
			</div>
		);
	}
});
