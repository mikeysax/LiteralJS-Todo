/** @jsx h */

import { render, component, h } from 'literaljs';
import './index.scss';

// Components
import Main from './components/Main';

const AppLayout = component({
	render() {
		return (
			<div class="App">
				<Main />
			</div>
		);
	}
});

// Default State
const state = {};

render(AppLayout, 'root', state);
